# Steps for generating time series constituents (TS) when sims already exist (eg 2020/2021 Report Cards).  
1.0 Get RDATA file of MPA, soil, climate etc from shp file.  
  * 1.1 Run GetSptData.R on windows and point to baseline and change shape files from Mel. (eg file name "RC12_Burdekin_baseline.RData""). No RPIT_Path for MW and WT  
  * 1.2 Check.eg areas in each mgt class (ie column SIMNAME) are intended  
  * 1.3 Copy RData files to \\athenasmb\scratchapsim (which is "/scratch/apsim/" on linux HPC)  
2.0 Aggregate farm output to sub-catchment (SC) for each constituent (eg DINrunoff, soil_loss) - SCAgg.R  
  * 2.1 Make changes to SCAgg.R appropriate for region (e.g. see section "# 0 set some constants") and save as different file (eg SCAgg_BU.R). Farm files are located in each regions directory in /scratch/apsim/RC11/  
  * 2.2 Run new SCAgg.R (if Region == "BU" or "BM" run on Athena88 else use Athena61)  
  *   * 2.2.1 If error msg "Missing farm files." Generate and run missing paddock files (Simgen.R) and aggregate to Farms (FarmAgg.R) using old clone of repo in /scratch/apsim/RC11/sugarcane  
  *   * 2.2.2 Ensure sims per PBS job does not cause walltime to exceed 100 hours
3.0 Check each time series constituents for negatives etc (eg check_SCOutput.R)  




# Key functions in R/Functions.R

**gen_combosDF(Region)** - generate a data.frame of sim combos with a sim per row (combosDF)  
Region - management region, must match a sub-directory in Regions/, eg "Burdekin"

**getMgtData(region,shpFileDir,layer,MgtMatrixPath,constituent,RPIT_Path=NULL)** - merge DAF managment data and climate and soil layers  
region - management region, adds a region column to output 
shpFileDir - dir containing shape file (exclude trailing '/') 
layer - shape file (exclude '.shp' as per rgdal)  
MgtMatrixPath - file name and path to DAF management csv  
constituent - SOILNUT or PEST  
scenario - scenario for subsetting daf management matrix
RPIT_Path - Recycling pit csv file name and path (NULL if not used)  



**gen_combosDF(Region)** - generate a data.frame of sim combos with a sim per row (combosDF)  
Region - management region, must match a sub-directory in Regions/, eg "Burdekin"

**gen_Sims(combos,simtFileName,simFileDir,Region,parallel=F)** - generate APSIM sim files for each row of combos  
combos - data.frame of sim combos returned from gen_combosDF  
simtFileName - simulation template file include path eg "Regions/Burdekin/template.simt"  
simFileDir - directory to write sim files (created if doesn't exist, include trailing '/')  
Region - management region, must match a sub-directory in Regions/, eg "Burdekin"  
parallel - generate sims in parallel (T or F)  

**Sims2PBS(simFileDir,PBSDirs,delPBSDirs = T)** - copy sim, met and qsub files to PBSDirs  
simFileDir - directory containing sim files  
PBSDirs - vector of destination subdirectories in /scratch/apsim/ to copy files. Number of PBS jobs equals length of PBSDirs. Jobs submitted on HPC via ssh and qsub apsim_pbs.qsub eg:  
for ((i=1; i<=2; i++ ))
do
echo BU_test$i
cd /scratch/apsim/PBS/BU_test$i
qsub apsim_pbs.qsub
done

**gen_FarmFiles(PaddockOutputDirs,FarmFiles,FarmOutputDir,Region,PaddockFactors,FarmFactors)** - aggregate paddock files to farms. See TSgen.R for example
PaddockOutputDirs - vector of directories containing paddock output (eg PBSDirs from Sims2PBS)  
FarmFiles - vector of Farm files (eg u(APSIM_DT$FarmFname)). Can be used to subset.
FarmOutputDir - where to write farm files  
Region - management region, must match a sub-directory in Regions/, eg "Burdekin"
PaddockFactors,FarmFactors - vectors of Paddock and Farm factors in order as they appear in file names

**gen_constituentTS(SC,APSIM_DT,constituents,FarmOutputDir,AREAS,Region,TSoutputDir)**  - aggregate farm files to sub-catchments (SC) for each constituents  
SC - vector of sub-catchments
APSIM_DT - DAF spatial data (eg output from primary/RScripts/GBRF_scens.R)  
constituents -  list of constituents to aggregate (eg runoff, soil_loss, ...)  
FarmOutputDir - dir containing farm files  
Region - management region (sub-directory or Regions/)  
AREAS - areas of polygon_ID corresponding to APSIM_DT (eg output from primary/RScripts/GBRF_scens.R)  
TSoutputDir - directory where to write output  


  
