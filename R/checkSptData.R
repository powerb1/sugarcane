# checks of DAF management layer  -------------
#rm(list=ls())

suppressMessages(source("R/Functions.R"))

Region <- "MackayWhitsundays"  # one of the 4 cane Regions
RCnumber <- "12"
constituent <- "SOILNUT"

# load  baseline and change data
scenario <- "baseline"
(load(paste0("./../RC12/MW",RCnumber,"_",Region,"_",scenario,".RData")))
B_DT <- u(APSIM_DT[,.(polygonID,PROP,SIMNAME)])
B_DT[,scen:="baseline"]

scenario <- "change"
(load(paste0("./../RC12/MW",RCnumber,"_",Region,"_",scenario,".RData")))
C_DT <- u(APSIM_DT[,.(polygonID,PROP,SIMNAME)])
C_DT[,scen:="change"]

dt0 <- rbind(B_DT,C_DT)
setnames(dt0,"SIMNAME","P2RClass")

dt0 <- splitP2RClass(dt0)


# add areas 
dt0 <- merge(dt0,AREAS,by="polygonID")

# checks   ------

# PROP sum to 1 for each polygonID and scenario
dt1 <- dt0[,.(PROP=sum(PROP)),by=c("polygonID","scen")]
u(dt1$PROP)

dt0 <- melt(dt0,id.vars = c( "SOWREG", "SUBCATS", "PROP","scen","AREA"),
            measure.vars = c("SoillClass","NutClass","MudClass"),
            variable.name = "Mgt",value.name = "MgtClass")

# AREAs in each class
dt0 <-  dt0[,.(Area=sum(AREA * PROP)),by=c("SOWREG", "SUBCATS","scen","Mgt","MgtClass")]


# add  change in area  from baseline to change
dt0 <- dcast(dt0,"SOWREG + SUBCATS + Mgt + MgtClass ~ scen",value.var = "Area")
dt0[,dlt_Area:=change - baseline]
#dt0[,P_change:=(change - baseline)/baseline]

# Check that change in area is close to zero
dt1 <- dt0[dlt_Area != 0,]
dt2 <- dt1[,.(dltArea=sum(dlt_Area)),by=.(SOWREG,SUBCATS,Mgt)]
max(dt2$dltArea)

#Check that MgtClass is going from a lower to a higher class (check = TRUE)
dt3 <-
  dt1 %>% 
  group_by(SOWREG, SUBCATS, Mgt) %>% 
  mutate(Project = ifelse(dlt_Area<0, 'Before', 'After')) %>% 
  select(-baseline, -change, -dlt_Area) %>% 
  pivot_wider(names_from="Project", values_from="MgtClass") %>% 
  mutate(check = map2(Before, After, ~ all(.x > .y, na.rm = TRUE)))

View(dt3)

# dltArea - net change in areas from baseline to change (should be very small or zero)
dt1 <- dt0[dlt_Area != 0,]
dt2 <- dt1[,.(dltArea=sum(dlt_Area)),by=.(SOWREG,SUBCATS,Mgt)]

#check directoion of change (eg Cf to Bp)

dt1[,MgtClass:=factor(MgtClass,ordered = T,levels = c("Af","Ap","Bf","Bp", "Cf", "Cp", "Df"))]

checkMgtOrder <- function(d0) diff(as.numeric(d1[order(dlt_Area,decreasing = T),MgtClass])) > 0
dt2 <- dt1[,.(CorrectOrder=checkMgtOrder(.SD)),by=.(SOWREG,SUBCATS,Mgt)]
all(dt2$CorrectOrder)


