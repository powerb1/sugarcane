# Prints progress of FarmAgg.R every 5 mins (or so)

rm(list=ls())
suppressMessages(source("R/Functions.R"))

CaneRegion <- "Burdekin" # one of the 4 cane regions
Region <- "Burdekin" # must match a sub-directory in Regions/
APSIM_DT <- loadBothSptDataScenarios(CaneRegion)
APSIM_DT <-  APSIM_DT[SOWREG != "Black",]
FarmOutputDir <- "/scratch/apsim/RC11/BU/Farm_with_irrig/"

FarmFiles <- unique(APSIM_DT$FarmFname)

while (T) {
  doneFarmFiles <- dir(FarmOutputDir)
  print(paste(Region,  round(length(doneFarmFiles)/ length(FarmFiles),2)))
  Sys.sleep(60 * 5)
}