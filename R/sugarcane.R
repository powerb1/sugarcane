# rm(list=ls())

# to do:
# check burdekin KS hack in replaceSoilParameters

Region <- "MackayWhitsundays" # must match a sub-directory in Regions/

suppressMessages(source("R/Functions.R"))
plan(multisession,workers=32) # run to setup local parallel processes


# generate combosDF (matrix with a sim per row) ----------
combosDF <- gen_combosDF(Region)

# subset combosDF example ------
subCombos <- expand.grid(list(SoilName="coom",
                              MetCode="2165-14935",
                              Soil="C",
                              Nutrient="C",
                              sowDates="May"),
                          stringsAsFactors = F)
combosDF <- inner_join(subCombos,combosDF)

# genate dir (outputDir) of APSIM sim files
simFileDir <- "../scratch/apsim/PBS/MW_test/" # directory to write sim files (created if doesn't exist, include trailing '/')
simtFileName <- paste0("Regions/", Region, "/template.simt") 
gen_Sims(combosDF,simtFileName,simFileDir,Region,parallel=F)

# run sims locally ----
simFileDir <- "../RegionsRC9/MW_50/"
APSIM10path <- "\"C:/Program Files (x86)/APSIM710-r4191/Model/ApsimModel.exe\""
#file.copy("Regions/Met/1725-14595.met",outputDir)
sims <- dir(simFileDir,".*sim$")
# remove done sims


#lapply(paste0(APSIM10path," ",outputDir,"/",sims),system,wait = F,invisible = F) # run all concurrently 
runAPSIMbatFiles(sims,8,simFileDir) # combine into n jobs
file.remove(dir(pattern = "bat$"))


# run on HPC using PBS #####
PBSDirs <- paste0("MW_test",1) 
Sims2PBS(simFileDir,PBSDirs,delPBSDirs = T)

## submit PBS jobs via ssh 
for ((i=1; i<=2; i++ ))
do
echo BU_test$i
cd /scratch/apsim/PBS/BU_test$i
qsub apsim_pbs.qsub
done






