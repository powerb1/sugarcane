# generate sim files 
# rm(list=ls())

suppressMessages(source("R/Functions.R"))
plan(multisession,workers=4) # run to setup local parallel forked processes
print(paste("nbrOfWorkers =",nbrOfWorkers()))

# set some constants ------
CaneRegion <- "MackayWhitsundays" # one of the 4 cane regions
Region <- "MackayWhitsundays"     # must match a sub-directory in Regions/

# factor names in order they appear in paddock and farm file names (BU includes IrrigationFreq)
Factors <- list(PaddockFactors=c("SoilName","P2RClass","MetCode","startYear","CaneRegion","sowDate","SOWREG"),
                FarmFactors=c("SoilName","MetCode","P2RClass","SOWREG"))

# gen combosDF
scenarios <- fread("Regions/Burdekin/ScenariosTest.csv")
combosDF <- gen_combosDF(Region)

# Subset combosDF and Generate sims 
simFileDir <- "/scratch/apsim/PBS/MW/"
all(file.remove(dir(simFileDir,"*.sim",full.names = T)))
all(file.remove(dir(simFileDir,"*.csv",full.names = T)))
all(file.remove(dir(simFileDir,"*.sum",full.names = T)))
simtFileName <- paste0("Regions/", Region, "/template.simt")

#combosDF <- combosDF[combosDF$simFiles == "garb_AfAfAfAfAf_1980-14740_1960_Burdekin_LowKS_Apr_BRIA",]

subCombos <- expand.grid(list(sowDate="Apr",
                              #IrrigCode=c("Cf"),
                              TillageCode="TillC",
                              FertCode="FertC",
                              StartYear=1920,
                              MillMudCode="Af"),
                              #SoilName="2Uge",
                              #SOWREG="BRIA"),
                          stringsAsFactors = F)


combosDF1 <- inner_join(subCombos,combosDF)
dim(combosDF1)

# Generate sims
gen_Sims(h(combosDF1),simtFileName,simFileDir,Region,parallel=F)

# Run locally with
sims <- dir(simFileDir,".*sim$")
APSIM10path <- "\"C:/Program Files (x86)/APSIM710-r4191/Model/ApsimModel.exe\""
#APSIM10path <- "\"C:/Program Files (x86)/APSIM710-r4191/Model/Apsim.exe\""

file.copy(unique(paste0("Regions/Met/",str_split(sims,"_",simplify = T)[,3],".met")),simFileDir)
lapply(paste0(APSIM10path," ",simFileDir,"/",sims),system,wait = F,invisible = F)

# Or, Run locally using bat files see RunSimsLocally.R

# Or Run on HPC see below
# Copy sims and qsub file to PBSDirs
#PBSDirs <- paste0("BU_",7:12)
#Sims2PBS(simFileDir,PBSDirs,delPBSDirs = T)

#for ((i=7; i<=12; i++ ))
#do
#qsub BU_$i/apsim_pbs.qsub
#done

