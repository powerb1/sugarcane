# compare BU soil_loss outputs 

suppressMessages(source("R/Functions.R"))
plan(multisession,workers=16) # run to setup local parallel forked processes
print(paste("nbrOfWorkers =",nbrOfWorkers()))

current_Dirs <- paste0("/scratch/apsim/PBS/BU_0_",1:10)
not_current_Dirs <- paste0("/scratch/apsim/PBS/BU_1_",1:10)

f1 <- function(outFile) {
  setDTthreads(1)
  #outFile <- outFiles[1]
  APSIM <- fread(outFile,skip=4,header = F)
  setnames(APSIM,names(data.table::fread(outFile,skip=2,header = T,nrows = 0)))
  APSIM <- APSIM[,.(soil_loss=sum(soil_loss))]
  APSIM[,outFile:=str_remove(outFile,"^/.*/")]
  APSIM
}

Current_outFiles <- unlist(lapply(current_Dirs,dir,pattern="csv",full.names=T))
Current_dt <- rbindlist(future_lapply(Current_outFiles,f1))
setnames(Current_dt,"soil_loss","Current_soil_loss")
notCurrent_outFiles <- unlist(lapply(not_current_Dirs,dir,pattern="csv",full.names=T))                 
Not_Current_dt <- rbindlist(future_lapply(notCurrent_outFiles,f1))
setnames(Not_Current_dt,"soil_loss","Previous_soil_loss")

outFiles <- unlist(lapply(paste0("/scratch/apsim/PBS/BU_2_",1:10),dir,pattern="csv",full.names=T))
dt_2 <- rbindlist(future_lapply(outFiles,f1))
setnames(dt_2,"soil_loss","Next_soil_loss")

outFiles <- unlist(lapply(paste0("/scratch/apsim/PBS/BU_3_",1:10),dir,pattern="csv",full.names=T))
dt_3 <- rbindlist(future_lapply(outFiles,f1))
setnames(dt_3,"soil_loss","Next_soil_loss")
#table(dt_3$soil_loss)


soiLoss_dt <- merge(merge(Current_dt,Not_Current_dt),dt_3)

soiLoss_dt[,Pchange:=(Current_soil_loss- Previous_soil_loss)/Current_soil_loss]
soiLoss_dt[,SoilCode:=str_sub(outFile,6,7)]
soiLoss_dt[,FertCode:=str_sub(outFile,8,9)]
soiLoss_dt[,MMCode:=str_sub(outFile,12,13)]
soiLoss_dt[,IrrigCode:=str_sub(outFile,14,15)]

soiLoss_dt1 <- melt(soiLoss_dt,
                    id.var=c("outFile","SoilCode","FertCode","MMCode","IrrigCode"),
                    measure.vars = c("Current_soil_loss","Previous_soil_loss","Next_soil_loss"),
                    variable.name="Source",
                    value.name = "soil_loss")

write_rds(soiLoss_dt1,"/scratch/apsim/soiLoss_dt1.RDS")

g1 <- ggplot(soiLoss_dt1,aes(SoilCode,soil_loss)) + 
  geom_boxplot(aes(colour = Source))
ggsave("../plots/BU SC #1642 soil loss.png")                    

getYields <- function(outFile) {
  outFile <- paste0("/scratch/apsim/PBS/BU_2_output/","garb_BpAfAfAfCf_1990-14775_1960_Burdekin_LowKS_Apr_delta.csv")
  setDTthreads(1)
  APSIM <- fread(outFile,skip=4,header = F)
  setnames(APSIM,names(data.table::fread(outFile,skip=2,header = T,nrows = 0)))
  APSIM[,fname:=str_remove(outFile,"^/.*/")]
  APSIM[,Date:=as.Date(todaydate,"%d/%b/%Y")]
  APSIM[,year:=format(Date,"%Y")]
  APSIM[,ratoon_no:=as.character(ratoon_no)]
  APSIM[,SugarIndex:=findInterval(Date,.SD[DAS == 0,Date]),by="fname"]
  #APSIMdt[sugar_crop_status == "out",SugarIndex:=0]
  APSIM <- APSIM[,fixCaneYields(.SD),by=c("fname","SugarIndex")]
  APSIM[currentCrop == "sugar", lapply(.SD,sum,na.rm=T), by=.(SugarIndex,fname),.SDcols = "CaneYield"]
}

outFiles <- unlist(lapply(paste0("/scratch/apsim/PBS/BU_0_",1:10),dir,pattern="csv",full.names=T))
dt_0 <- rbindlist(future_lapply(outFiles,getYields))
setnames(dt_0,"CaneYield","CaneYield_0")

outFiles <- unlist(lapply(paste0("/scratch/apsim/PBS/BU_1_",1:10),dir,pattern="csv",full.names=T))
dt_1 <- rbindlist(future_lapply(outFiles,getYields))
setnames(dt_1,"CaneYield","CaneYield_1")

yields_dt <- merge(dt_1,dt_0)
yields_dt <- melt(yields_dt,
                    id.var=c("fname"),
                    measure.vars = c("CaneYield_1","CaneYield_0"),
                    variable.name="Source",
                    value.name = "CaneYield")

write_rds(yields_dt,"/scratch/apsim/yields_dt.RDS")

#outFiles <- unlist(lapply(paste0("/scratch/apsim/PBS/BU_1_",1:10),dir,pattern="csv",full.names=T))
outFiles <- unlist(lapply("/scratch/apsim/PBS/BU_2_output/",dir,pattern="csv",full.names=T))
dt_2 <- rbindlist(future_lapply(outFiles,getYields))
u(dt_2$CaneYield)
setnames(dt_2,"soil_loss","Next_soil_loss")

CurrentYields_dt <- rbindlist(future_lapply(Current_outFiles,getYields))
NotCurrentYields_dt <- rbindlist(future_lapply(notCurrent_outFiles,getYields))



                    
######################################################################################





setwd("/scratch/apsim/sugarcane/")
library(tidyverse)

PBSDirs <- paste0("/scratch/apsim/PBS/BU_",17:30,"/")

f1 <- function(PBSDir) {
  #PBSDir <- PBSDirs[1]
  print(PBSDir)
  simFiles <- str_remove(dir(PBSDir,"sim$"),".sim$")
  print(paste("sim files:",length(simFiles)))
  csvFiles <- str_remove(dir(PBSDir,"csv$"),".csv$")
  print(paste("csv files:",length(csvFiles)))
  deleted <- file.remove(paste0(PBSDir,intersect(simFiles,csvFiles),".sim"))
  all(deleted)
}

lapply(PBSDirs,f1)

PBSDirs <- paste0("/scratch/apsim/PBS/BU_",1:30,"/")
lapply(PBSDirs,function(PBSDir) print(paste(PBSDir,"csv files:",length(dir(PBSDir,"csv$")))))



