#rm(list=ls())
#Script to get and plot soil parameters.
library(xml2)

suppressMessages(source("R/Functions.R"))
#plan(multisession,workers=4) # run to setup local parallel forked processes
#print(paste("nbrOfWorkers =",nbrOfWorkers()))

SoilFile <- "Regions/Burdekin/Soils.soils"

# returns a data.table of all soil paramaters from a soil file (eg soils.soil)
getSoilParameters <- function(SoilFile) {
  soils <- read_xml(SoilFile)
  SoilList <- list()
  
  for (i1 in 1:length(xml_children(soils))) {
    #i1 <- 1
    soil <- xml_child(soils,i1)
    soilName <- xml_attr(soil,"name")
    for (i2 in 1:length(xml_children(soil))) {
      #i2 <- 6
      soilChild <- xml_child(soil,i2)
      if (xml_length(soilChild) == 0) next
      for (i3 in 1:length(xml_children(soilChild))) {
         #i3 <- 8
         soilGrandChild <- xml_child(soilChild,i3)
         if (xml_length(soilGrandChild) > 0) {
           Value4 <- vector(mode="character",length = length(xml_children(soilGrandChild)))
           for (i4 in 1:length(xml_children(soilGrandChild))) {
             #i4 <- 1
             SoilGreatGrandChild <- xml_child(soilGrandChild,i4)
             if (xml_length(SoilGreatGrandChild) > 0) {
               Value5 <- vector(mode="character",length = length(xml_children(SoilGreatGrandChild)))
               for (i5 in 1:length(xml_children(SoilGreatGrandChild))) {
                 Value5[i5] <- xml_text(xml_child(SoilGreatGrandChild,i5))
               }
               SoilList[[paste(i1,i2,i3,i4,sep="_")]] <- data.table(Soil=soilName,
                                                              Variable=paste(xml_attr(soilGrandChild,"name"),xml_name(SoilGreatGrandChild),sep = "_"),
                                                              Value=Value5)
             } else {
               Value4[i4] <- xml_text(xml_child(soilGrandChild,i4))
               SoilList[[paste(i1,i2,i3,sep="_")]] <- data.table(Soil=soilName,
                                                              Variable=xml_name(soilGrandChild),
                                                              Value=Value4)
             }
           }
         } else {
           Value <- xml_text(soilGrandChild)
           SoilList[[paste(i1,i2,i3,sep="_")]] <- data.table(Soil=soilName,
                                                          #SoilComponent=xml_attr(soilChild,"name"),
                                                          Variable=xml_name(soilGrandChild),
                                                          Value=Value)
         } 
      }
    }
  }
  rbindlist(SoilList)
}  

Soils1 <- getSoilParameters(SoilFile)
Soils1[,Source:="New"]
Soils0 <- getSoilParameters("../primary/Regions/Burdekin/Soils.soils")
Soils0[,Source:="Old"]

soils <- rbind(Soils1,Soils0)

soils <- soils[,Value:=as.numeric(Value)]
soils <- soils[!is.na(Value)]


for (var in u(soils$Variable)) {
  #var <- u(soils$Variable)[1]
  gg1 <- ggplot(soils[Variable == var,],aes(x=Source,y=as.numeric(Value))) +
    geom_point(aes(colour=Soil)) +
    labs(y=var)
  ggsave(paste("../Plots/BU Soil/",var,".png"))
}
  

gg1 <- ggplot(soils[Variable == "FBiom",],aes(x=Source,y=as.numeric(Value))) +
  geom_point(aes(colour=Soil)) +
  labs(y="FBiom")





  


      
      
         

         
             
           
           
           xml_siblings(soilGrandChild)
           x1 <- unclass(xml_contents(soilGrandChild))
           str()
           
           
           
              xml_find_num(soilGrandChild,xpath = "//double",)
         }
            
         
         
         
         SoilList[[xml_name(soilChild)]][[j]] <- 

         
         xml_name(soilChild)
      
      xml_child(soilChild)
      xml_contents(soilChild)
      lapply(soilChild,xml_child)
      
      xml_text(soilChild)
      
      
      str_which(unclass(xml_children(soil)),"water")
      
      xml_child()
    }
    
#xml_validate(soils,read_xml("Regions/Burdekin/Soils.soils"))

for (i in 1:12) {
  #i <- 1
  water <- xml_child(xml_child(soils,i),6)
  print(xml_name(water))
  mungs <- xml_child(water,9)
  print(xml_attr(mungs,"name"))
  cowpea <- mungs
  xml_add_sibling(mungs,cowpea)
  xml_set_attr(cowpea, "name", "cowpea")
}

#xml_child(xml_child(soils,7),6)

write_xml(soils,"Regions/Burdekin/Soils.soils")


