#rm(list=ls())

suppressMessages(source("R/Functions.R"))
plan(multisession,workers=16) # run to setup local forked parallel processes
print(paste("nbrOfWorkers =",nbrOfWorkers()))

CaneRegion <- "Burdekin" # one of the 4 cane regions
Region <- "Burdekin" # must match a sub-directory in Regions/
#Region <- "Black" # must match a sub-directory in Regions/

(load("/scratch/apsim/WQT_Burdekin_baseline.RData"))


# BU
APSIM_DT[APSIMSoil == "Rugb",APSIMSoil:="RUgb"]
APSIM_DT <- APSIM_DT[SOWREG!="other",]
APSIM_DT[SOWREG=="Delta",SOWREG:="delta"]
APSIM_DT <- APSIM_DT[SOWREG != "Black"]
APSIM_DT[,FarmFname:=paste0(paste(APSIMSoil,APSIMClim,P2RClass,SOWREG,sep="$"),".csv")]
#APSIM_DT <- APSIM_DT[SUBCATS == "SC #107"]
#APSIM_DT <- APSIM_DT[SUBCATS == "SC #1627"]
# SCsize <- table(APSIM_DT$SUBCATS)
# names(SCsize)[order(SCsize)]
APSIM_DT <- APSIM_DT[order(PROP,decreasing = T)]
PROP1 <- u(APSIM_DT$PROP)
APSIM_DT <- copy(APSIM_DT[PROP >= PROP1[3000],])

#SoilName    MetCode P2RClass SOWREG
#1:     Rugb 1980-14740 S6N4M1I8  delta
#u(APSIM_DT$APSIMSoil)
#s1 <- fread("Regions/Burdekin/Scenarios.csv")
#u(s1$SoilName)


# factor names in order they appear in paddock and farm file names (BU includes IrrigationFreq)
Factors <- list(PaddockFactors=c("SoilName","P2RClass","MetCode","startYear","CaneRegion","sowDate","SOWREG"),
                FarmFactors=c("SoilName","MetCode","P2RClass","SOWREG"))

# dirs of paddock output
#PaddockOutputDirs <- paste0("/scratch/apsim/PBS/WQT_BU_",c(1:500,601))
PaddockOutputDirs <- paste0("/scratch/apsim/PBS/BU_",1:501)
  

# 3.2 aggregate multiple paddock files (startyear and sowdate) to farm files
FarmOutputDir <- "/scratch/apsim/RC13/BU/Farm/"

#names(freadAPSIM(dir(PaddockOutputDirs[1],"csv",full.names = T)[1]))[5]
#tail(freadAPSIM(dir(PaddockOutputDirs[1],"csv",full.names = T)[1])[,5])

freadAPSIMargs <- list(incompleteRuns=paste0(Region,"_incompleteRuns.csv"),
                       dateCol=5,
                       #dateCol=10, #black
                       lastDate=as.Date("2023-06-30"), # error thrown if last date < this
                       useSOWREG=T) 

# check for incomplete paddock files
#incompletePaddockFiles <- getIncompletePaddockFiles(PaddockOutputDirs,freadAPSIMargs$lastDate,pattern="csv$",freadAPSIMargs$dateCol) 
#if (length(incompletePaddockFiles) > 0) all(file.remove(incompletePaddockFiles))
#length(incompletePaddockFiles)/length(unlist(lapply(PaddockOutputDirs,dir,"csv$",full.names=T))) * 100
#h(incompletePaddockFiles)


# x1 <- dir(PaddockOutputDirs,full.names = T)
# x2 <- future_sapply(x1,getLastDate)
# LastDates_dt <- data.table(LastDate = as.Date(x2,"%d/%b/%Y"),
#                            PaddockFiles=names(x2))
# sum(file.remove(LastDates_dt[LastDate < freadAPSIMargs[["lastDate"]],PaddockFiles]))

# check necessary paddock files exist
paddockFiles <- unlist(lapply(PaddockOutputDirs,dir,"csv$",full.names=T))
Paddock_dt <- as.data.table(str_split(str_remove(paddockFiles, "\\/.*\\/"),"[_\\.\\/]",simplify = T))
Paddock_dt <- Paddock_dt[,1:length(Factors$PaddockFactors)]
setnames(Paddock_dt,Factors$PaddockFactors)
Paddock_dt[,P2RClass:=str_remove(P2RClass,"P[1-8]")]
# # # #Paddock_dt[SoilName =="thor" & MetCode == "1910-14645" & SOWREG == "Black" & P2RClass == "S2N6P4M1"]
# # # 
Farm_dt <- as.data.table(str_split(str_remove(APSIM_DT$FarmFname, "\\/.*\\/"),"[\\$\\.\\/]",simplify = T))
Farm_dt <- Farm_dt[,1:length(Factors$FarmFactors)]
setnames(Farm_dt,Factors$FarmFactors)
#missingFarmFiles <- anti_join(Farm_dt,Paddock_dt)
#Farm_dt1 <- merge(Farm_dt,u(Paddock_dt[,Factors$FarmFactors,with=F]))
#Farm_dt1[,FarmFname:=paste0(paste(SoilName,MetCode,P2RClass,SOWREG,sep="$"),".csv")]

aggColNames <- getAggColNames(Region)
aggColNames <- str_subset(aggColNames,"PestLoss",negate = T)

gen_FarmFiles(PaddockOutputDirs,u(APSIM_DT$FarmFname),FarmOutputDir,Region,Factors,freadAPSIMargs,checkForCompleteSets = T,aggColNames =aggColNames,Parallel = T) 


#length(dir(FarmOutputDir))


