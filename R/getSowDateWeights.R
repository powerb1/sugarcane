
suppressMessages(source("R/Functions.R"))

# BU ----------------------------
(load("/scratch/apsim/RC11_Burdekin_baseline.RData"))

#black
sowDtaeWeights <- fread("Regions/Black/SowDateWeights.csv")
sowDtaeWeights[,SOWREG:="Black"]
sowDtaeWeights <- fwrite(sowDtaeWeights,"Regions/Black/SowDateWeights.csv")


# BM -------------
# Mels's scenarios.CSV provides mapping from Burrum, Baffle, Burnett, Kolan, Mary to 
# Bundaberg, Isis, Mary

(load("A:/RC11_Burnett_baseline.RData"))

Scens_0 <- u(APSIM_DT[,.(APSIMSoil,APSIMPerm,APSIMClim,SOWREG)])
setnames(Scens_0,c("SoilName","KS","MetCode","SOWREG_0"))

ScenDT <- rbind(fread("Regions/Burnett/Scenarios.csv"),
                fread("Regions/Maryborough/Scenarios.csv"))

dim(ScenDT)
dim(u(Scens_0))

ScenDT_1 <- merge(ScenDT,Scens_0,all=T)

sowDateWeights <- rbind(data.table(SOWREG="Bundaberg",
                                   sowDate = c("Feb","Mar","Apr","Aug","Sep"),
                                   sowDateWeights=c(0.1125,0.27,0.0675,0.275,0.275)),
                        data.table(SOWREG="Isis",
                                   sowDate = c("Mar","Apr","Aug","Sep"),
                                   sowDateWeights=c(0.055,0.025,0.2,0.72)),
                        data.table(SOWREG="Maryborough",
                                   sowDate = c("Mar","Apr","Aug","Sep"),
                                   sowDateWeights=c(0.1,0.1,0.4,0.4)))

# add sowDate Weights 
ScenDT_1 <- merge(ScenDT_1,sowDateWeights,by="SOWREG",allow.cartesian=T)

sowDatesDT <- u(ScenDT_1[,.(SOWREG_0,MetCode,sowDate,sowDateWeights)])
setnames(sowDatesDT,"SOWREG_0","SOWREG")
fwrite(u(sowDatesDT[SOWREG != "Mary"]),"Regions/Burnett/SowDateWeights.csv")
fwrite(u(sowDatesDT[SOWREG == "Mary"]),"Regions/Maryborough/SowDateWeights.csv")

# save scenarios
ScenM <- fread("Regions/Maryborough/Scenarios.csv")
ScenDT <- ScenDT_1[,.(SoilName,KS,MetCode,SOWREG_0)]
setnames(ScenDT,"SOWREG_0","SOWREG")

fwrite(u(ScenDT[SOWREG != "Mary"]),"Regions/Burnett/Scenarios.csv")
fwrite(u(ScenDT[SOWREG == "Mary"]),"Regions/Maryborough/Scenarios.csv")






# WT ------------

# wetTropic 
(load("/scratch/apsim/RC11_WT_baseline.RData"))

SOWREGsowDateWeights <- rbindlist(list(
  data.table(sowDate = c("Jun","Jul","Aug","Sep"),
             sowDateWeights=c(0.06,0.40,0.40,0.14),
             SOWREG="Johnstone"),
  data.table(sowDate = c("Jun","Jul","Aug","Sep"),
             sowDateWeights=c(0.06,0.40,0.40,0.14),
             SOWREG="Daintree-Mossman"),
  data.table(sowDate = c("Jun","Jul","Aug","Sep"),
             sowDateWeights=c(0.32,0.3,0.3,0.08),
             SOWREG = "Mulgrave-Russell"),
  data.table(sowDate = c("May","Jun","Jul","Aug","Sep"),
             sowDateWeights=c(0.05,0.2,0.3,0.3,0.15),
             SOWREG="Tully-Murray")))

#MetCodes_dt <- unique(APSIM_DT[RepReg %in% c("Tully","Mulgrave-Russell","Johnstone"),.(APSIMClim,SOWREG)])
#MetCodes_dt[APSIMClim == "1750-14585",]
#sowDateWeights <- merge(RepRegsowDateWeights,MetCodes_dt,allow.cartesian = T,all = T)
#setnames(sowDateWeights,"APSIMClim","MetCode")
#sowDateWeights[,RepReg:=NULL]
fwrite(SOWREGsowDateWeights,"Regions/WetTropics/SowDateWeights.csv")

# herbert  

# tableland
