    <component name="Pesticide" executable="%apsim%\Model\Manager2.dll" class="Manager2">
      <executable name="%apsim%/Model/Manager2.dll" version="1.0" />
      <initdata>
        <Manager2 />
        <ui>
          <A type="text" description="Param A">Hello</A>
        </ui>
        <text>//CTRL + ML to collapse all outlining
//CTRL + KD to format code
//CTRL + KC to comment selected block
//CTRL + KU to uncomment selected block
using System;
using ModelFramework;
using CSGeneral;

public class Script 
{      
   [Link] Paddock MyPaddock; // Can be used to dynamically get access to simulation structure and variables
   [Input] DateTime Today;   // Equates to the value of the current simulation date - value comes from CLOCK
   
   #region General
   public double sugarDAS;
   public string sugarStatus;
   public double sugarCover;
   public double cropCover;
   public int ratoon_no;
   public double fallow_flag;
   public double DAS; 
   public string soilCode = "";
   public string pestmgmt;
   public string tillageCode; 
   
   public double rain;
   public double maxt;
   public double mint; 
   public double runoff;
   public double surfaceCover;

   #endregion
   
   #region Pesticides
   [Output] public double PSII_PestLossInRunoffWater_g_per_ha;
   [Output] public double PSII_PestLossOnRunoffSed_g_per_ha;
   [Output] public double PSII_TotalPestLossInRunoff_g_per_ha;
   
   [Output] public double pest_MixingLayerThickness;
   [Output] public double pest_ExtractionCoefficient;
   public double soil_loss;
   [Output] public double pest_SedDelivRatio;

   [Output] public double pest_stubdegrate;
   [Output] public double pest_soildegrate;
   [Output] public double pest_porosity;
   [Output] public double pest_Exponentialfunc;
   [Output] public double pest_infiltration;
   [Output] public double pest_denom1;
   [Output] public double pest_vegdegrate;
   [Output] public double pest_Kd;
   [Output] public double pest_denom2;
   [Output] public double pest_sediment_conc;
   
   [Output] public double CoverWashOffFraction;
   [Output] public double RainWashoffCoefficient;   
   [Output] public double PestOnStubble_g_per_ha;
   [Output] public double sorpBYext;
   
   [Output] public double HalfLifeVeg_adjusted;
   [Output] public double ReferenceHalfLifeVeg;
   [Output] public double HalfLifeStubble_adjusted;
   [Output] public double Ref_AirTemperatureSoil_kelvin;
   [Output] public double ReferenceTemperatureSoil;
   [Output] public double HalfLifeSoil_adjusted;
   [Output] public double ReferenceHalfLifeSoil;
   [Output] public double ActivationEnergy;
   [Output] public double UniversalGasConstant;
   [Output] public double pest_AirTemperature_kelvin;
   [Output] public double AppliedPestOnStubble_g_per_ha;
   [Output] public double pest_denom;
   [Output] public double availwaterstorageinmixing;
   
   [Output] public double pest_crop_cover;
   [Output] public double pest_stubble_cover;
   [Output] public String ApplicationPosition;
   [Output] public double ReferenceTemperatureVegetation;
   [Output] public double Ref_AirTemperatureVeg_kelvin;
   
   [Output] public double IngredConcentration;
   [Output] public double ApplicationEffeciency;
   [Output] public double pest_Rate;
   [Output] public double pest_BandingPercent;
   [Output] public double pest_DrainedUpperLimit;
   [Output] public double pest_WiltingPoint;
   [Output] public double pest_UpperSoilLayerDepth;
   [Output] public double pest_BulkDensity;
   

   $PestManagerVariables$
   
   #endregion
   
   public bool doneSetup = false;
   
   // The following event handler will be called once at the beginning of the simulation
   [EventHandler] public void OnInitialised()
   {

   }
     
   // The following event handler will be called each day at the beginning of the day
   //Start of Day
   [EventHandler] public void OnPrepare()
   {
      MyPaddock.Get("soilCode", out soilCode);
      MyPaddock.Get("pestmgmt", out pestmgmt);
      MyPaddock.Get("tillageCode", out tillageCode);  
      
      if(soilCode != "")
      {
         initPesticideModel();
      }
   }
   
   // The following event handler will be called each day at the end of the day
   //End of Day
   [EventHandler] public void OnPost()
   {
      //These are here to replicate bug where Apsim 73 is using yesterdays variables
      MyPaddock.Get("fallow_flag", out fallow_flag);
      MyPaddock.Get("DAS", out DAS);
      MyPaddock.Get("sugar.DaysAfterSowing", out sugarDAS);
      MyPaddock.Get("sugar.crop_status", out sugarStatus);
      MyPaddock.Get("sugar.ratoon_no", out ratoon_no);
	  MyPaddock.Get("runoff", out runoff);
      MyPaddock.Get("paddock.Erosion.soil_loss",out soil_loss);
   }
   
   // The following event handler will be called each day 
   [EventHandler] public void OnProcess()
   {
      if(doneSetup)
      {
	     //string [] pesticides = new string[] {$PesticidesList$};
		 //string[] pesticides = new string[] {"Diuron","Atrazine","Hexazinone","Ametryn","Isoxaflutole","Metribuzin","Imazapic"};
         //RunPesticideModel(pesticides);
	
	     MyPaddock.Get("fallow_flag", out fallow_flag);
       MyPaddock.Get("DAS", out DAS);
         MyPaddock.Get("sugar.DaysAfterSowing", out sugarDAS);
         MyPaddock.Get("sugar.crop_status", out sugarStatus);
         MyPaddock.Get("sugar.ratoon_no", out ratoon_no);
	     MyPaddock.Get("runoff", out runoff);
         MyPaddock.Get("paddock.Erosion.soil_loss",out soil_loss);
		 
		 //pesticides = new string[] {"twofourD","Glyphosate","Paraquat"};
		 string[] pesticides = new string[] {$PesticidesList$};
		 
		 RunPesticideModel(pesticides);
      }
   }
   
   #region Utilities
   public void getDailyVars()
   {
      //MyPaddock.Get("sugar.DaysAfterSowing", out sugarDAS);
      //MyPaddock.Get("sugar.crop_status", out sugarStatus);
      //MyPaddock.Get("sugar.ratoon_no", out ratoon_no);
      MyPaddock.Get("sugar.cover_tot", out sugarCover);
      MyPaddock.Get("$LegumeCrop$.cover_tot", out cropCover);
      MyPaddock.Get("rain", out rain);
      MyPaddock.Get("maxt", out maxt);
      MyPaddock.Get("mint", out mint);
      //MyPaddock.Get("runoff", out runoff);
      MyPaddock.Get("surfaceom_cover", out surfaceCover);
      
	  //Fix bug for cowpea cover   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Mel to check why this was here, commented out 
	  //cropCover = 0;
	  
	  //Fix bug to line up sugar DAS
	  //sugarDAS = System.Math.Max(sugarDAS - 1, 0);
	  
   }
   
   public DateTime date(string dateString)
   {
      DateTime dt = new DateTime();
      //convert the date string to APSIM date
      string[] split = dateString.Split(new char[]{'-', '/'});
      int month = Convert.ToInt32(split[1]);
      int day = Convert.ToInt32(split[0]);
      
      String[] months = new String[] { null, "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec" };

      string newDateString = day.ToString("00") + "-" + months[month];
      
      if (split.Length == 3)
      {
         int year = Convert.ToInt32(split[2]);
         dt = DateUtility.GetDate(newDateString, year);
      }
      else
      {
         dt = DateUtility.GetDate(newDateString);
      }
   
      return dt;
   }
   
   public void setInternalFieldValue(string fieldName, object val)
   {
      Type thisType = this.GetType();
      System.Reflection.FieldInfo field = thisType.GetField(fieldName);
     
      field.SetValue(this, val);
   }
   
   public double getInternalFieldValue(string fieldName)
   {
      Type thisType = this.GetType();
      System.Reflection.FieldInfo field = thisType.GetField(fieldName);
     
      return (double) field.GetValue(this);
   }
   #endregion
   
   
  
   public void initPesticideModel()
   {
      
      if(doneSetup)
      {
         return;
      }
      ////////////////////Howleaky Pesticide Model ////////////////////////////////////////////  ;
      ////////////////////////////////////////////////////////////Generic pesticide parameters;
      pest_MixingLayerThickness = 25;
      pest_ExtractionCoefficient = 0.02;

      $PestManagerProperties$
      ////////////////////////////////////////////////////////////Pesticide specific product and physchem parameters
      
      $PestSoilKD$
        
      doneSetup = true;
   }
   
   public void RunPesticideModel(string[] pesticides)
   {
      getDailyVars();
      
      PSII_PestLossInRunoffWater_g_per_ha = 0;
      PSII_PestLossOnRunoffSed_g_per_ha = 0;
      PSII_TotalPestLossInRunoff_g_per_ha = 0;
      
      UniversalGasConstant = 8.314472;
      pest_AirTemperature_kelvin = ((maxt + mint) / 2) + 273.15;
      
      for(int i = 0; i &lt; pesticides.Length; i++)
      {
         string currentPest = pesticides[i];
         
         if ((Today &gt;= date("1/1/1900") &amp;&amp; Today &lt;= date("2/1/1907")) || Today &gt;= date("1/1/1980") )
         {
            ////////////////////////////////////////////////////////////Pesticide applications - DIURON;
            IngredConcentration = getInternalFieldValue(currentPest + "_IngredConcentration");
            ApplicationEffeciency = getInternalFieldValue(currentPest + "_ApplicationEffeciency");
            ReferenceTemperatureVegetation = getInternalFieldValue(currentPest + "_ReferenceTemperatureVegetation");
            ReferenceHalfLifeVeg = getInternalFieldValue(currentPest + "_ReferenceHalfLifeVeg");
            ReferenceTemperatureSoil = getInternalFieldValue(currentPest + "_ReferenceTemperatureSoil");
            ReferenceHalfLifeSoil = getInternalFieldValue(currentPest + "_ReferenceHalfLifeSoil");
            ActivationEnergy = getInternalFieldValue(currentPest + "_ActivationEnergy");
            CoverWashOffFraction = getInternalFieldValue(currentPest + "_CoverWashOffFraction");
            RainWashoffCoefficient = getInternalFieldValue(currentPest + "_RainWashoffCoefficient");
            pest_Kd = getInternalFieldValue(currentPest + "_Kd");
            
            //Set before overides
            pest_Rate = 0;
            pest_BandingPercent = 100;
            ApplicationPosition = "BelowCanopyAboveMulch";

            //Calculate the rates - this will call for exampe "calcDiuronRates"
            Type thisType = this.GetType();
            System.Reflection.MethodInfo theMethod = thisType.GetMethod("calc" + currentPest + "Rates");
            theMethod.Invoke(this, null);
         
            //calculate amount pesticide applied to crop, stubble&amp;&amp; soil depending on application position;
            pest_crop_cover = sugarCover + cropCover;
            pest_stubble_cover = (1 - pest_crop_cover) * surfaceCover;
            //MyPaddock.Set(currentPest + "_total_application", IngredConcentration * pest_Rate * pest_BandingPercent / 100 * ApplicationEffeciency / 100);
         
            double pest_total_application = IngredConcentration * pest_Rate * pest_BandingPercent / 100 * ApplicationEffeciency / 100;
         
            setInternalFieldValue(currentPest + "_total_application", pest_total_application);
         
            double pest_AppliedPestOnVeg_g_per_ha = 0;
            double pest_AppliedPestOnStubble_g_per_ha = 0;
            double pest_AppliedPestOnSoil_g_per_ha = 0;

            double pest_washedoffVeg_g_per_ha = 0;
            double pest_washedoffStubble_g_per_ha = 0;
            
            if( ApplicationPosition == "AboveCanopy" )
            {
               pest_AppliedPestOnVeg_g_per_ha = pest_total_application * pest_crop_cover;
               pest_AppliedPestOnStubble_g_per_ha = pest_total_application * pest_stubble_cover;
               pest_AppliedPestOnSoil_g_per_ha = pest_total_application - pest_AppliedPestOnVeg_g_per_ha - pest_AppliedPestOnStubble_g_per_ha;
            }
            else if( ApplicationPosition == "BelowCanopyAboveMulch" )
            {
               pest_AppliedPestOnVeg_g_per_ha = 0;
               pest_AppliedPestOnStubble_g_per_ha = pest_total_application * pest_stubble_cover;
               pest_AppliedPestOnSoil_g_per_ha = pest_total_application - pest_AppliedPestOnVeg_g_per_ha - pest_AppliedPestOnStubble_g_per_ha;
            }
            else if( ApplicationPosition == "DirectToSoil" )
            {
               pest_AppliedPestOnVeg_g_per_ha = 0;
               pest_AppliedPestOnStubble_g_per_ha = 0;
               pest_AppliedPestOnSoil_g_per_ha = pest_total_application - pest_AppliedPestOnVeg_g_per_ha - pest_AppliedPestOnStubble_g_per_ha;
            }
            
            setInternalFieldValue(currentPest + "_AppliedPestOnVeg_g_per_ha", pest_AppliedPestOnVeg_g_per_ha);
            setInternalFieldValue(currentPest + "_AppliedPestOnStubble_g_per_ha", pest_AppliedPestOnStubble_g_per_ha);
            setInternalFieldValue(currentPest + "_AppliedPestOnSoil_g_per_ha", pest_AppliedPestOnSoil_g_per_ha);

            //calculate degrading pesticide on veg, stubble&amp;&amp; soil;
            Ref_AirTemperatureVeg_kelvin = ReferenceTemperatureVegetation + 273.15;
            HalfLifeVeg_adjusted = ReferenceHalfLifeVeg * Math.Pow(2.718281828, ((ActivationEnergy / UniversalGasConstant) * (1 / pest_AirTemperature_kelvin - 1 / Ref_AirTemperatureVeg_kelvin)));
            pest_vegdegrate = Math.Pow(2.718281828, (-0.693 / HalfLifeVeg_adjusted));
            HalfLifeStubble_adjusted = ReferenceHalfLifeVeg * Math.Pow(2.718281828, ((ActivationEnergy / UniversalGasConstant) * (1 / pest_AirTemperature_kelvin - 1 / Ref_AirTemperatureVeg_kelvin)));
            pest_stubdegrate = Math.Pow(2.718281828, (-0.693 / HalfLifeStubble_adjusted));
            Ref_AirTemperatureSoil_kelvin = ReferenceTemperatureSoil + 273.15;
            HalfLifeSoil_adjusted = ReferenceHalfLifeSoil * Math.Pow(2.718281828, ((ActivationEnergy / UniversalGasConstant) * (1 / pest_AirTemperature_kelvin - 1 / Ref_AirTemperatureSoil_kelvin)));
            pest_soildegrate = Math.Pow(2.718281828, (-0.693 / HalfLifeSoil_adjusted));
            
            double pest_PestOnVeg_g_per_ha = getInternalFieldValue(currentPest + "_PestOnVeg_g_per_ha");
            double pest_PestOnStubble_g_per_ha = getInternalFieldValue(currentPest + "_PestOnStubble_g_per_ha");
            double pest_PestInSoil_g_per_ha = getInternalFieldValue(currentPest + "_PestInSoil_g_per_ha");
            double pest_PestLossInLeaching_g_per_ha = getInternalFieldValue(currentPest + "_PestLossInLeaching_g_per_ha");
            double pest_TotalPestLossInRunoff_g_per_ha = getInternalFieldValue(currentPest + "_TotalPestLossInRunoff_g_per_ha");
            
            double pest_PestConcInSoil_mg_per_kg;
            
            if (rain &lt; 1 )
            {
               pest_PestOnVeg_g_per_ha = pest_PestOnVeg_g_per_ha * pest_vegdegrate + pest_AppliedPestOnVeg_g_per_ha;
               pest_PestOnStubble_g_per_ha = pest_PestOnStubble_g_per_ha * pest_stubdegrate + pest_AppliedPestOnStubble_g_per_ha;
               pest_PestInSoil_g_per_ha = pest_PestInSoil_g_per_ha * pest_soildegrate + pest_AppliedPestOnSoil_g_per_ha - pest_PestLossInLeaching_g_per_ha - pest_TotalPestLossInRunoff_g_per_ha;
            }
            else if( rain &gt;= 1 )
            {
               //pest_PestOnVeg_g_per_ha = (pest_PestOnVeg_g_per_ha * pest_vegdegrate + pest_AppliedPestOnVeg_g_per_ha) * (1 - CoverWashOffFraction);
               //pest_PestOnStubble_g_per_ha = (pest_PestOnStubble_g_per_ha * pest_stubdegrate + AppliedPestOnStubble_g_per_ha) * (1 - CoverWashOffFraction);
               //pest_PestInSoil_g_per_ha = pest_PestInSoil_g_per_ha * pest_soildegrate + pest_AppliedPestOnSoil_g_per_ha - pest_PestLossInLeaching_g_per_ha - pest_TotalPestLossInRunoff_g_per_ha + ((pest_PestOnStubble_g_per_ha + pest_PestOnVeg_g_per_ha) * CoverWashOffFraction);
             
               //RZWQM washoff equation (Wauchope et al 2004, Pest Management Science)
               pest_washedoffVeg_g_per_ha = (pest_PestOnVeg_g_per_ha * pest_vegdegrate + pest_AppliedPestOnVeg_g_per_ha) - ((pest_PestOnVeg_g_per_ha * pest_vegdegrate + pest_AppliedPestOnVeg_g_per_ha) * CoverWashOffFraction * Math.Pow(2.718281828, (-RainWashoffCoefficient * rain)));
               pest_washedoffStubble_g_per_ha = (pest_PestOnStubble_g_per_ha * pest_stubdegrate + AppliedPestOnStubble_g_per_ha) - ((pest_PestOnStubble_g_per_ha * pest_stubdegrate + AppliedPestOnStubble_g_per_ha) * CoverWashOffFraction * Math.Pow(2.718281828, (-RainWashoffCoefficient * rain)));
               pest_PestOnVeg_g_per_ha = (pest_PestOnVeg_g_per_ha * pest_vegdegrate + pest_AppliedPestOnVeg_g_per_ha) * CoverWashOffFraction * Math.Pow(2.718281828, (-RainWashoffCoefficient * rain));
               pest_PestOnStubble_g_per_ha = (pest_PestOnStubble_g_per_ha * pest_stubdegrate + AppliedPestOnStubble_g_per_ha) * CoverWashOffFraction * Math.Pow(2.718281828, (-RainWashoffCoefficient * rain));
               pest_PestInSoil_g_per_ha = pest_PestInSoil_g_per_ha * pest_soildegrate + pest_AppliedPestOnSoil_g_per_ha - pest_PestLossInLeaching_g_per_ha - pest_TotalPestLossInRunoff_g_per_ha + pest_washedoffVeg_g_per_ha + pest_washedoffStubble_g_per_ha;
            }
                                              
            //calculate pesticide concentration in soil;
            pest_denom = pest_BulkDensity * pest_MixingLayerThickness * 10;
            if (pest_denom &gt; 0) {
               pest_PestConcInSoil_mg_per_kg = pest_PestInSoil_g_per_ha / pest_denom;
            }
            else {
               pest_PestConcInSoil_mg_per_kg = 0;
            }
    
            setInternalFieldValue(currentPest + "_PestOnVeg_g_per_ha", pest_PestOnVeg_g_per_ha);
            setInternalFieldValue(currentPest + "_PestOnStubble_g_per_ha", pest_PestOnStubble_g_per_ha);
            setInternalFieldValue(currentPest + "_PestInSoil_g_per_ha", pest_PestInSoil_g_per_ha);
            setInternalFieldValue(currentPest + "_PestConcInSoil_mg_per_kg", pest_PestConcInSoil_mg_per_kg);
            
            //calculate infiltration;
            availwaterstorageinmixing = (pest_DrainedUpperLimit - pest_WiltingPoint) * pest_MixingLayerThickness / pest_UpperSoilLayerDepth;
            pest_infiltration = rain - runoff - availwaterstorageinmixing;
            if (pest_infiltration &lt;= 0 )
            {
               pest_infiltration = 0;
            }
            else {
               pest_infiltration = rain - runoff - availwaterstorageinmixing;
            }
            
            double pest_PestConcInSoilAfterLeach_mg_per_kg;
            
            //calculate denominator of the PestConcInSoilAfterLeaching equation; 
            pest_porosity = 1 - pest_BulkDensity / 2.65;
            pest_denom1 = pest_MixingLayerThickness * (pest_Kd * pest_BulkDensity + pest_porosity);
            pest_Exponentialfunc = Math.Pow(2.718281828, ((pest_infiltration * -1) / pest_denom1));
            if ((pest_PestConcInSoil_mg_per_kg &gt; 0) &amp;&amp; (pest_denom1 &gt; 0) )
            {
               pest_PestConcInSoilAfterLeach_mg_per_kg = pest_PestConcInSoil_mg_per_kg * pest_Exponentialfunc;
            }
            else {
               pest_PestConcInSoilAfterLeach_mg_per_kg = 0;
            }

            setInternalFieldValue(currentPest + "_PestConcInSoilAfterLeach_mg_per_kg", pest_PestConcInSoilAfterLeach_mg_per_kg);

            //calculate pesticide runoff concentrations;
            sorpBYext = pest_Kd * pest_ExtractionCoefficient;
            pest_denom2 = (1 + sorpBYext);
            pest_SedDelivRatio = 1;
            if (runoff &gt; 0 )
            {
               pest_sediment_conc = soil_loss * 100 / runoff * pest_SedDelivRatio;
            }
            else {
               pest_sediment_conc = 0;
            }
            
            double pest_PestConcInRunoffWater_ug_per_l = 0;
            double pest_PestConcOnRunoffSed_mg_per_kg = 0;
            double pest_TotalPestConcInRunoff_ug_per_l = 0;
            double pest_PestLossInRunoffWater_g_per_ha = 0;
            double pest_PestLossOnRunoffSed_g_per_ha = 0;
            double pest_PestConcLeached_mg_per_kg = 0;

            if ((runoff &gt;= 0) &amp;&amp; (pest_PestConcInSoil_mg_per_kg &gt;= 0) &amp;&amp; (pest_denom2 &gt;= 0) )
            {
               pest_PestConcInRunoffWater_ug_per_l = pest_PestConcInSoilAfterLeach_mg_per_kg * pest_ExtractionCoefficient / pest_denom2 * 1000;
               pest_PestConcOnRunoffSed_mg_per_kg = pest_PestConcInSoilAfterLeach_mg_per_kg * sorpBYext / pest_denom2;
               pest_TotalPestConcInRunoff_ug_per_l = pest_PestConcInRunoffWater_ug_per_l + pest_PestConcOnRunoffSed_mg_per_kg * pest_sediment_conc;
            }
            else {
               pest_PestConcInRunoffWater_ug_per_l = 0;
               pest_PestConcOnRunoffSed_mg_per_kg = 0;
               pest_TotalPestConcInRunoff_ug_per_l = 0;
            }

            //calculate pesticide losses;
            if(runoff &gt; 0 )
            {
               pest_PestLossInRunoffWater_g_per_ha = pest_PestConcInRunoffWater_ug_per_l * runoff * 0.01;
               pest_PestLossOnRunoffSed_g_per_ha = pest_PestConcOnRunoffSed_mg_per_kg * soil_loss * pest_SedDelivRatio;
               pest_TotalPestLossInRunoff_g_per_ha = pest_PestLossInRunoffWater_g_per_ha + pest_PestLossOnRunoffSed_g_per_ha;
            }
            else if( runoff &lt;= 0 )
            {
               pest_PestLossInRunoffWater_g_per_ha = 0;
               pest_PestLossOnRunoffSed_g_per_ha = 0;
               pest_TotalPestLossInRunoff_g_per_ha = 0;
            }

            pest_PestConcLeached_mg_per_kg = pest_PestConcInSoil_mg_per_kg - pest_PestConcInSoilAfterLeach_mg_per_kg;
            if (pest_PestConcLeached_mg_per_kg &lt;= 0 )
            {
               pest_PestLossInLeaching_g_per_ha = 0;
            }
            else {
               pest_PestLossInLeaching_g_per_ha = pest_PestConcLeached_mg_per_kg * pest_BulkDensity * pest_MixingLayerThickness / 10;
            }
            
            setInternalFieldValue(currentPest + "_PestConcInRunoffWater_ug_per_l", pest_PestConcInRunoffWater_ug_per_l);
            setInternalFieldValue(currentPest + "_PestConcOnRunoffSed_mg_per_kg", pest_PestConcOnRunoffSed_mg_per_kg);
            setInternalFieldValue(currentPest + "_TotalPestConcInRunoff_ug_per_l", pest_TotalPestConcInRunoff_ug_per_l);
            setInternalFieldValue(currentPest + "_PestLossInLeaching_g_per_ha", pest_PestLossInLeaching_g_per_ha);
            setInternalFieldValue(currentPest + "_PestLossInRunoffWater_g_per_ha", pest_PestLossInRunoffWater_g_per_ha);
            setInternalFieldValue(currentPest + "_PestLossOnRunoffSed_g_per_ha", pest_PestLossOnRunoffSed_g_per_ha);
            setInternalFieldValue(currentPest + "_TotalPestLossInRunoff_g_per_ha", pest_TotalPestLossInRunoff_g_per_ha);
            setInternalFieldValue(currentPest + "_PestConcLeached_mg_per_kg", pest_PestConcLeached_mg_per_kg);
            
            if ((Today &gt;= date("1/1/1900") &amp;&amp; Today &lt;= date("2/1/1907")) || Today &gt;= date("1/1/1980"))   
            {
               PSII_PestLossInRunoffWater_g_per_ha += pest_PestLossInRunoffWater_g_per_ha;
               PSII_PestLossOnRunoffSed_g_per_ha += pest_PestLossOnRunoffSed_g_per_ha;
               PSII_TotalPestLossInRunoff_g_per_ha += pest_TotalPestLossInRunoff_g_per_ha;
            }
         }
      }
   }  
   
   #region Individual Pest positions
   $PestManagementApplications$
   
   #endregion
}
       </text>
      </initdata>
    </component>
